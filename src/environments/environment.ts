// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB5XNvuHSTLiHIqNR680he2GWdk5wcVAgI",
    authDomain: "bd-dochealt.firebaseapp.com",
    databaseURL: "https://bd-dochealt-default-rtdb.firebaseio.com/",
    projectId: "bd-dochealt",
    storageBucket: "bd-dochealt.appspot.com",
    messagingSenderId: "1000296561883",
    appId: "1:1000296561883:web:5e6bbdc3aafd8310b6a0fe",
    measurementId: "G-H07RGPJRYX"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
