import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from "@angular/forms";
import { NoteService } from './../shared/note.service';

@Component({
  selector: 'app-newnote',
  templateUrl: './newnote.page.html',
  styleUrls: ['./newnote.page.scss'],
})
export class NewnotePage implements OnInit {
  bookingForm: FormGroup;

  constructor(private aptService: NoteService,
    private router: Router,
    public fb: FormBuilder) { }

  ngOnInit() {
    this.bookingForm = this.fb.group({
      title: [''],
      contenu: ['']
    })
  }

  formSubmit() {
    if (!this.bookingForm.valid) {
      return false;
    } else {
      this.aptService.createBooking(this.bookingForm.value).then(res => {
        console.log(res)
        this.bookingForm.reset();
        this.router.navigate(['/notebook']);
      })
        .catch(error => console.log(error));
    }
  }

}
