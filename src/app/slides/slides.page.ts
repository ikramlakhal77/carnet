import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-slides',
  templateUrl: './slides.page.html',
  styleUrls: ['./slides.page.scss'],
})
export class SlidesPage  {

  constructor(public alertController: AlertController ) { }
  
  showAlert() {

    this.alertController.create({
      header: 'Création du compte',
      subHeader: ' ',
      message: 'Pour le premier lancement',
      buttons: ['CONTINUER']
    }).then(res => {

      res.present();

    });

  }

}
