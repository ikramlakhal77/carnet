import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from "@angular/forms";
import { AppointmentService } from './../shared/appointment.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-make-appointment',
  templateUrl: './make-appointment.page.html',
  styleUrls: ['./make-appointment.page.scss'],
})

export class MakeAppointmentPage implements OnInit {
  bookingForm: FormGroup;

  constructor(
    public alertController: AlertController,
    private aptService: AppointmentService,
    private router: Router,
    public fb: FormBuilder
  ) { }
  showAlert() {

    this.alertController.create({
      header: 'Vous avez crée un nouveau contact',
      buttons: ['ok']
    }).then(res => {

      res.present();

    });

  }
  ngOnInit() {
    this.bookingForm = this.fb.group({     
      name: [''],
      prenom: [''],
      structure: [''],
      fix: [''],
      portable: [''],
      fax: [''],
      adresse: [''],
      email: [''],
      site: [''],
      note: ['']
    })
  }

  formSubmit() {
    if (!this.bookingForm.valid) {
      return false;
    } else {
      this.aptService.createBooking(this.bookingForm.value).then(res => {
        console.log(res)
        this.bookingForm.reset();
        this.router.navigate(['/contact']);
      })
        .catch(error => console.log(error));
    }
  }
}

