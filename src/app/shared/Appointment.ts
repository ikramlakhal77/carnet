export class Appointment {
    $key: string;
    name: string;
    prenom: string;
    structure: string;
    fix: number;
    portable: number;
    fax: number;
    adresse: string;
    email: string;
    site: string;
    note: string;
    } 